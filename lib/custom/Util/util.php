<?php
/**
 * Copyright (c) 2019. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.04.2019
 * Time: 14:42
 */

/**
 * @param $filepath путь который нужно создать если его нет.
 */

function createPath($filepath) {
    try {
        $filepath = str_replace("\\", "/", $filepath);
        $isInFolder = preg_match("/^(.*)\/([^\/]+)$/", $filepath, $filepathMatches);
        if($isInFolder) {
            $folderName = $filepathMatches[1];

            if (!is_dir($folderName)) {
                mkdir($folderName, 0777, true);
            }
        }
    } catch (Exception $e) {
        echo "ERR: error writing '$filepath', ". $e->getMessage();
    }
}

function getCachePath() {
    return platformSlashes(ucfirst($_SERVER['DOCUMENT_ROOT']) . "/wa-cache/apps/site/webp" . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) . ".webp");
}

function platformSlashes($path) {
    if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
        $path = str_replace('/', '\\', $path);
    }
    return $path;
}