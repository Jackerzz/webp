<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.04.2019
 * Time: 10:35
 */

class WebpConverterImagick implements WebpConverter
{
    public function convert($file, $destination, $quality)
    {
        createPath($destination);
        $imagick = new Imagick();
        $imagick->readImage($file);
        $imagick->setImageFormat("webp");
        $imagick->setImageCompressionQuality($quality);
        $imagick->writeImage($destination);
    }
}