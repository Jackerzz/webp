<?php
/**
 * Created by PhpStorm.
 * User: Максим
 * Date: 04.04.2019
 * Time: 10:34
 */

class WebpConverterGD implements WebpConverter
{

    public function convert($file, $destination, $quality)
    {
        createPath($destination);
        $image_object = $this->getImageResource($file);
        imagewebp($image_object,$destination, $quality);
    }

    /**
     * @param $filepath
     * @return resource|null
     * @throws Exception
     */
    private function getImageResource($filepath) {
        $mime_type = mime_content_type ($filepath);

        $image_object = null;

        switch ($mime_type) {
            case "image/gif":
                $image_object = imagecreatefrompng($filepath);
                break;
            case "image/png":
                $image_object = imagecreatefrompng($filepath);
                break;
            case "image/jpeg":
                $image_object = imagecreatefromjpeg($filepath);
                break;
        }

        if(!$image_object) {
            throw new Exception('Unsupported format: "' . $mime_type . '" at: ' . $filepath);
        }
        return $image_object;
    }

}