<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.04.2019
 * Time: 10:23
 */

interface WebpConverter
{
    /**
     * @param $file файл источник
     * @param $destination путь сохранения файла
     * @param $quality качество
     * @return void
     * @throws Exception
     * Записывает сконвертированный $file в $destination
     */
    public function convert($file, $destination,$quality);
}