<?php


include_once ('autoload.php');

$image_path = platformSlashes(ucfirst($_SERVER['DOCUMENT_ROOT']) . $_SERVER['REQUEST_URI']);

if(!file_exists($image_path)) {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
    exit;
}

$cache_path = getCachePath();

if(file_exists($cache_path)) {
    header("Content-Type:image/webp");
    exit(file_get_contents($cache_path));
}

$converter = new WebpConverterGD();
try {
    $converter->convert($image_path, $cache_path, 80);
} catch (Exception $e) {
    error_log($e->getMessage());
    header("Content-Type:" . mime_content_type($image_path));
    exit(file_get_contents($image_path));
}
header("Content-Type:image/webp");
exit(file_get_contents($cache_path));
